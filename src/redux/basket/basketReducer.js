import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from "./basketTypes"

const initialState = {
	products: [
	],
}

const setproduct = (products, payload) => {
	let exist = false
	const newproducts = products.reduce( (acc, el) => {
		if (el.name === payload.name){
			exist = true
			el.number = String(Number(payload.number) + Number(el.number))
		}
		  acc.push(el)
		return acc
	}, [])
	return ( exist ? newproducts : [...newproducts, payload])
}

const removeproduct = (products, name) => {
	const newproducts = products.reduce( (acc, el) => {
		if (el.name !== name){
			//el.number = String(Number(payload.number) + Number(el.number))
			acc.push(el)
		}
		return acc
	}, [])
	return newproducts
}

const basketReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TO_BASKET:
			return {
				//products: [...state.products, action.payload],
				products: [...setproduct(state.products, action.payload)]
			}
		case REMOVE_FROM_BASKET:
			return {
				products: [...removeproduct(state.products, action.payload)]
			}

		default:
			return state
	}
}

export default basketReducer
