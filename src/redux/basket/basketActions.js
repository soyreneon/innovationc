import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from "./basketTypes"

export const addToBasket = product => {
	return {
		type: ADD_TO_BASKET,
		payload: product,
	}
}

export const removeFromBasket = productname => {
	return {
		type: REMOVE_FROM_BASKET,
		payload: productname,
	}
}
