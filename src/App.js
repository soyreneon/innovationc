import "./App.css"
import AddToBasket from "./components/AddtoBasket"
import ListProducts from "./components/ListProducts"
import { Provider } from "react-redux"
import store from "./redux/store"

function App() {
	return (
		<div className="App">
			<Provider store={store}>
				<AddToBasket />
				<ListProducts />
			</Provider>
		</div>
	)
}

export default App
