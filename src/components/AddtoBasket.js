import { useState } from "react"
import { connect } from "react-redux"
import { addToBasket } from '../redux'

const AddToBasket = ({addToBasket}) => {
	const [name, setName] = useState("producto 1")
	const [price, setPrice] = useState(3.5)
	const [number, setnumber] = useState("2")
	const [id, setID] = useState(0)
	const onClickBasket = e => {
        e.preventDefault()
        setID( oldid => ++oldid)
		const productobj = {
            id, name, price, number
        }
		//console.log(name)
        //console.log(productobj)
        addToBasket(productobj)
	}
	return (
		<div className="addbasket">
			<form onSubmit={(e) => onClickBasket(e)}>
				<input
					defaultValue="producto 1"
					onChange={e => setName(e.target.value)}
                    type='text'
					name="name"
				/>
				<input
					defaultValue="3.5"
					onChange={e => setPrice(e.target.value)}
					name="price"
					type='number'
				/>
				<input
					defaultValue="2"
					onChange={e => setnumber(e.target.value)}
					name="numberOfProducts"
					type='number'
				/>
				<button>add to basket</button>
			</form>
		</div>
	)
}

const mapDispatchToProps = dispatch => {
	return {
		addToBasket: (product) => dispatch(addToBasket(product)),
	}
}

export default connect(null, mapDispatchToProps)(AddToBasket)
